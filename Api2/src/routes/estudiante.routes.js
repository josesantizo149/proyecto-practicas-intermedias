const { Router } = require('express');
const router = Router();

const estudianteControllers = require('../controllers/estudiante.controllers');

router.get('/test',estudianteControllers.test);
router.post('/createClase', estudianteControllers.createClase);
router.get('/clasesCreadas', estudianteControllers.ClaseCreadas);
router.post('/clasesAprobadas', estudianteControllers.ClasesAprobadas);
router.post('/infoClase', estudianteControllers.infoClase);
router.post('/InfoClaseSin',estudianteControllers.InfoClaseSin);
router.put('/updateEstablecimeinto', estudianteControllers.UpdateEstablecimiento);
router.post('/AddFoto', estudianteControllers.AddFoto);

module.exports = router;

/*
GET: enviar algo
POST: ingresos a la base de datos, registros de datos etc
DELETE: eliminar algo
PUT: actualizar algo
*/

//router.get('/setTurno',sillaControlers.setTurno);