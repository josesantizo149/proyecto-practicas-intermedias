const db = require('../database')
const estudianteControllers = {};
const aws_keys = require('../../Credenciales/cred');

var AWS = require('aws-sdk');

const {S3} = require('aws-sdk');

const s3 = new AWS.S3(aws_keys.s3);

//###########################################
//####### END POINT 1 #######################
//###########################################

estudianteControllers.test = async (req, res) => {
    db.query(
        'SELECT * FROM `ADMIN`',
        function (err, results) {
            return res.status(200).send(results);
        }
    );
}

estudianteControllers.createClase = async(req, res) => {
  var aprobado = 0;
  
  var clase = "insert into clase(nombre, descripcion,seccion, aprobado, id_establecimiento, id_catedratico) values('"+req.body.nombre+"','"+req.body.descripcion+"','"+req.body.seccion+"',"+aprobado+","+req.body.establecimiento+","+req.body.catedratico+")";

  console.log(clase)

  db.query(clase, function(err, results) {
    if(err){
      res.json({Text: "Erro al insertar"})
    }else{
      res.json({Text: "insertado"});
    }
  })
}

estudianteControllers.ClaseCreadas = async(req, res) => {

  var ClasesCreadas = 'SELECT * FROM clase';

  db.query(ClasesCreadas, function(err, results) {
    if(err){
      res.json({Text: "No se encuentran clases creadas"});
    }else{
      res.json({results})
    }
  });

}

estudianteControllers.ClasesAprobadas = async(req,res) => {

  var clasesAprobadas = 'select * from clase where aprobado = 1 order by nombre ' + req.body.order;

  db.query(clasesAprobadas, function(err, results) {
    if(err){
      res.json({Text: "No hay clases aprobadas"});
    }else{
      res.json({results});
    }
  });
};

estudianteControllers.infoClase = async(req, res) => {

  var InfoClase = 'select catedratico.nombre, catedratico.correo, count(estudiante.nombre) as CantidadEstudiantes, clase.nombre as NombreCurso from asignacion inner join clase on clase.id_clase = asignacion.id_clase inner join estudiante on estudiante.id_estudiante = asignacion.id_estudiante inner join catedratico on catedratico.id_catedratico = clase.id_catedratico where clase.id_clase = ' + req.body.clase;

  db.query(InfoClase, function(err, results) {
    if(err){
      res.json({Text: "Curso no encontrado"});
    }else{
      res.json({results});
    }
  });
};

estudianteControllers.InfoClaseSin = async(req, res) => {
  var info = 'select clase.nombre, clase.seccion, catedratico.nombre as NombreCatedratico, catedratico.apellido from clase inner join catedratico on catedratico.id_catedratico = clase.id_catedratico where id_clase = ' + req.body.clase;

  db.query(info, function(err, results) {
    if(err){
      res.json({Text: "No hay curso"});
    }else{ 
      res.json({results});
    }
  })
}


estudianteControllers.UpdateEstablecimiento = async(req, res) => {
  
  var nombre = req.body.nombre;
  var direccion = req.body.direccion;
  var tipo = req.body.tipo;
  var correo = req.body.correo;
  var contra = req.body.contra;
  var bibliografica = req.body.bibliografica;
  var escudo = req.body.escudo;

  var id_establecimiento = req.body.id_establecimiento;
  

  var nombrei = "establecimiento/" + nombre + ".jpg";

  //se convierte la base64 a bytes
  let buff = new Buffer.from(escudo, 'base64');


  
  const params = {
    Bucket: "proyectoiniciales",
    Key: nombrei,
    Body: buff,
    ContentType: "image",
    ACL: 'public-read'
  };


  s3.upload(params, (err, data)=>{
      if(err) {
          console.log(err);
          //res.status(404).send('puerka');
          res.status(404).send({'msg':'error'});
      }
          nombre =  nombre,
          direccion = direccion,
          tipo =  tipo,
          correo =  correo,
          contra = contra,
          bibliografica =  bibliografica,
          escudo = data.Location
          establecimiento = id_establecimiento

      
      db.query("update establecimiento SET nombre = '" + nombre + "', direccion = '" + direccion +"', tipo = '" + tipo + "', correo ='" + correo +"', contra =  '" + contra + "', bibliografia = '" + bibliografica + "', escudo = '" + escudo + "' where id_establecimiento = " + establecimiento, function (err, result) {
        if(err){
          console.log(err);
        }else{
          console.log("si", result);
        }
      });
      //res.status(200).send('listo papi');
      res.status(200).send({'msg':'listo papi'});
  });

  
}

estudianteControllers.AddFoto = async(req, res) => {
  var foto = req.body.foto;
  var id_establecimiento = req.body.id_establecimiento;
  var nombre = req.body.nombre;


  var nombrei = "establecimiento/" + nombre + ".jpg";

  //se convierte la base64 a bytes
  let buff = new Buffer.from(foto, 'base64');


  
  const params = {
    Bucket: "proyectoiniciales",
    Key: nombrei,
    Body: buff,
    ContentType: "image",
    ACL: 'public-read'
  };


  s3.upload(params, (err, data)=>{
      if(err) {
          console.log(err);
          //res.status(404).send('puerka');
          res.status(404).send({'msg':'error'});
      }
          
     
      foto =  data.Location,
      id_establecimiento =  id_establecimiento,
      nombre = nombre
     
      
      db.query("INSERT INTO foto_establecimiento(foto,id_establecimiento,nombre) values ('" + foto + "'," + id_establecimiento + ",'" + nombre + "')", function (err, result) {
        if(err){
          console.log(err);
        }else{
          console.log("si", result);
        }
      });
      //res.status(200).send('listo papi');
      res.status(200).send({'msg':'listo papi'});
  });

} 



module.exports = estudianteControllers;






































/*


  db.query(
    'INSERT INTO uso_silla (idUsuario, hora_inicio, hora_fin, peso, horas_uso, fecha_uso, distancia, dia) VALUES (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

  db.query(
    'call inUso (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

console.log(fecha.getDate()) //Dia del mes
console.log(fecha.getDay()) //D:0L:1M:2M:3J:4V:5S:6
console.log(fecha.getMonth()) //E:0F:1M:2A:3M:4J:5J:6A:7S:8O:9N:10D:11
console.log(fecha.getFullYear())
console.log(fecha.getHours())
console.log(fecha.getMinutes())
console.log(fecha.getSeconds())
console.log()
console.log()

fecha1
2021-09-08T04:32:29.228Z
1631075549228


fecha2
2021-09-08T04:43:04.399Z
1631076184399

635171

minutos (fecha2-fecha1)/60000

horas (fecha2-fecha1)/


min: 10.5861833
horas: 0.1764363883333


var fecha = new Date();

    var timestamp = fecha.getTime().toString()

    console.log(fecha)
    console.log(fecha.getTime())
    console.log(fecha.getDay())
    console.log(fecha.getMonth()+1)
    console.log(fecha.getFullYear())
    */

/*
var f1 = 1631075549228
var f2 = 1631076184399
var min = (f2 - f1)/60000
var hora = min/60
console.log(min)
console.log(hora)

*/