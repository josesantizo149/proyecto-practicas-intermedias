const db = require('../database')
const estudianteControllers = {};


//###########################################
//####### END POINT 1 #######################
//###########################################

estudianteControllers.test = async (req, res) => {
    return res.status(200).send({ results: 'exito' })
}

//########################################
//######## LOGINS ########################
//########################################

estudianteControllers.LoginAdministrador = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM ADMIN WHERE correo = ? AND contra = ?',
            [data.correo, data.password],
            function (err, results) {
                if (results.length !== 0) {
                    return res.status(200).send({ error: false, message: '', result: results })
                } else {
                    return res.status(500).send({ error: true, message: 'Las credenciales no son las correctas', result: null });
                }

            }
        )
    } catch (error) {
        console.log('Error en logeo de administrador')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.LoginEstablecimiento = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM establecimiento WHERE correo = ? AND contra = ?',
            [data.correo, data.password],
            function (err, results) {
                if (results.length !== 0) {
                    return res.status(200).send({ error: false, message: '', result: results })
                } else {
                    return res.status(500).send({ error: true, message: 'Las credenciales no son las correctas', result: null });
                }

            }
        )
    } catch (error) {
        console.log('Error en logeo de establecimiento')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.LoginCatedratico = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM catedratico WHERE correo = ? AND contra = ?',
            [data.correo, data.password],
            function (err, results) {
                if (results.length !== 0) {
                    return res.status(200).send({ error: false, message: '', result: results })
                } else {
                    return res.status(500).send({ error: true, message: 'Las credenciales no son las correctas', result: null });
                }

            }
        )
    } catch (error) {
        console.log('Error en logeo de catedratico')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.LoginEstudiante = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM estudiante WHERE correo = ? AND contra = ?',
            [data.correo, data.password],
            function (err, results) {
                if (results.length !== 0) {
                    return res.status(200).send({ error: false, message: '', result: results })
                } else {
                    return res.status(500).send({ error: true, message: 'Las credenciales no son las correctas', result: null });
                }

            }
        )
    } catch (error) {
        console.log('Error en logeo de estudiante')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//######## REGISTROS #####################
//########################################

estudianteControllers.RegistroAdministrador = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'INSERT INTO ADMIN(cui, nombre, apellido, correo, contra) VALUES (?,?,?,?,?)',
            [data.cui, data.nombre, data.apellido, data.correo, data.password],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en registro de administrador')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.RegistroEstablecimiento = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'INSERT INTO establecimiento(nombre, direccion, tipo, correo, contra, bibliografia, estado) VALUES (?,?,?,?,?,?,?)',
            [data.nombre, data.direccion, data.tipo, data.correo, data.password, data.bibliografia, data.estado],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en registro de establecimiento')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.RegistroCatedratico = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'INSERT INTO catedratico(cui, nombre, apellido, telefono, correo, contra, estado) VALUES (?,?,?,?,?,?,?)',
            [data.cui, data.nombre, data.apellido, data.telefono, data.correo, data.password, data.estado],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en registro de catedratico')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.RegistroEstudiante = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'INSERT INTO estudiante(cui, nombre, apellido, telefono, correo, contra, estado) VALUES (?,?,?,?,?,?,?)',
            [data.cui, data.nombre, data.apellido, data.telefono, data.correo, data.password, data.estado],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en registro de estudiante')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//######## SOLICITUDES ###################
//########################################

estudianteControllers.SolicitudEstablecimiento = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM establecimiento WHERE estado=?',
            ['pendiente'],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud establecimiento')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.SolicitudCatedratico = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM catedratico WHERE estado=?',
            ['pendiente'],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud catedratico')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.SolicitudEstudiante = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM estudiante WHERE estado=?',
            ['pendiente'],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud estudiante')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//######## ACCIONES ######################
//########################################

estudianteControllers.AccionEstablecimiento = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'UPDATE establecimiento SET estado = ? WHERE id_establecimiento = ?',
            [data.estado, data.id],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en accion establecimiento')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.AccionCatedratico = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'UPDATE catedratico SET estado = ? WHERE cui = ?',
            [data.estado, data.cui],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en accion catedratico')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

estudianteControllers.AccionEstudiante = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'UPDATE estudiante SET estado = ? WHERE cui = ?',
            [data.estado, data.cui],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en accion estudiante')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### ESTABLECIMIENTOS APROBADOS ########
//########################################

estudianteControllers.Establecimientos = async (req, res) => {
    try {
        const data = req.body
        db.query(
            'SELECT * FROM establecimiento WHERE estado=?',
            ['aprobado'],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud establecimiento aprobado')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}


//########################################
//#### ESTABLECIMIENTO ELEGIDO ###########
//########################################

estudianteControllers.Establecimiento = async (req, res) => {
    try {
        const params = req.params
        db.query(
            'SELECT * FROM establecimiento WHERE id_establecimiento=?',
            [params.id],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud establecimiento aprobado')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### CLASES ESTABLECIMIENTO ############
//########################################

estudianteControllers.ClasesEstablecimiento = async (req, res) => {
    try {
        const params = req.params
        db.query(
            'SELECT * FROM clase WHERE id_establecimiento=?',
            [params.id],
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud establecimiento aprobado')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### REPORTE 1 #########################
//########################################

estudianteControllers.reporte1 = async (req, res) => {
    try {
        db.query(
            `select count(*) as cantidad, establecimiento.nombre as establecimiento from clase
        inner join establecimiento on establecimiento.id_establecimiento = clase.id_establecimiento
        group by clase.id_establecimiento
        order by cantidad desc
        limit 5`,
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud reporte1')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### REPORTE 2 #########################
//########################################

estudianteControllers.reporte2 = async (req, res) => {
    try {
        db.query(
            `select count(*) as cantidad, establecimiento.nombre as establecimiento from asignacion
        inner join clase on clase.id_clase = asignacion.id_clase
        inner join establecimiento on establecimiento.id_establecimiento = clase.id_establecimiento
        group by asignacion.id_estudiante, clase.id_establecimiento
        order by cantidad desc
        limit 5`,
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud reporte2')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### REPORTE 3 #########################
//########################################

estudianteControllers.reporte3 = async (req, res) => {
    try {
        db.query(
            `select count(*) as cantidad, catedratico.nombre from clase
        inner join catedratico on catedratico.id_catedratico = clase.id_catedratico
        group by clase.id_catedratico
        order by cantidad desc
        limit 5`,
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud reporte3')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

//########################################
//#### REPORTE 4 #########################
//########################################

estudianteControllers.reporte4 = async (req, res) => {
    try {
        db.query(
            `select count(*) as cantidad, catedratico.nombre from asignacion
        inner join clase on clase.id_clase = asignacion.id_clase
        inner join catedratico on catedratico.id_catedratico = clase.id_catedratico
        group by asignacion.id_estudiante, clase.id_catedratico
        order by cantidad desc
        limit 5`,
            function (err, results) {
                if (err) {
                    return res.status(500).send({ error: true, message: err.sqlMessage, result: null });
                } else {
                    return res.status(200).send({ error: false, message: '', result: results })
                }
            }
        )
    } catch (error) {
        console.log('Error en solicitud reporte4')
        return res.status(500).send({ error: true, message: error.message, result: null });
    }
}

module.exports = estudianteControllers;

/*
 db.query(
        'SELECT * FROM `ADMIN`',
        function (err, results) {
            return res.status(200).send(results);
        }
    );
 */





































/*


  db.query(
    'INSERT INTO uso_silla (idUsuario, hora_inicio, hora_fin, peso, horas_uso, fecha_uso, distancia, dia) VALUES (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

  db.query(
    'call inUso (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

console.log(fecha.getDate()) //Dia del mes
console.log(fecha.getDay()) //D:0L:1M:2M:3J:4V:5S:6
console.log(fecha.getMonth()) //E:0F:1M:2A:3M:4J:5J:6A:7S:8O:9N:10D:11
console.log(fecha.getFullYear())
console.log(fecha.getHours())
console.log(fecha.getMinutes())
console.log(fecha.getSeconds())
console.log()
console.log()

fecha1
2021-09-08T04:32:29.228Z
1631075549228


fecha2
2021-09-08T04:43:04.399Z
1631076184399

635171

minutos (fecha2-fecha1)/60000

horas (fecha2-fecha1)/


min: 10.5861833
horas: 0.1764363883333


var fecha = new Date();

    var timestamp = fecha.getTime().toString()

    console.log(fecha)
    console.log(fecha.getTime())
    console.log(fecha.getDay())
    console.log(fecha.getMonth()+1)
    console.log(fecha.getFullYear())
    */

/*
var f1 = 1631075549228
var f2 = 1631076184399
var min = (f2 - f1)/60000
var hora = min/60
console.log(min)
console.log(hora)

*/