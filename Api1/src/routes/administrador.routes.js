const { Router } = require('express');
const router = Router();

const administradorControllers = require('../controllers/administrador.controllers');

router.get('/test', administradorControllers.test);

router.post('/login/administrador', administradorControllers.LoginAdministrador);
router.post('/login/establecimiento', administradorControllers.LoginEstablecimiento);
router.post('/login/catedratico', administradorControllers.LoginCatedratico);
router.post('/login/estudiante', administradorControllers.LoginEstudiante);

router.post('/registro/administrador', administradorControllers.RegistroAdministrador);
router.post('/registro/establecimiento', administradorControllers.RegistroEstablecimiento);
router.post('/registro/catedratico', administradorControllers.RegistroCatedratico);
router.post('/registro/estudiante', administradorControllers.RegistroEstudiante);

router.get('/solicitud/establecimiento', administradorControllers.SolicitudEstablecimiento);
router.get('/solicitud/catedratico', administradorControllers.SolicitudCatedratico);
router.get('/solicitud/estudiante', administradorControllers.SolicitudEstudiante);

router.put('/solicitud/accion/establecimiento', administradorControllers.AccionEstablecimiento);
router.put('/solicitud/accion/catedratico', administradorControllers.AccionCatedratico);
router.put('/solicitud/accion/estudiante', administradorControllers.AccionEstudiante);

router.get('/establecimientos', administradorControllers.Establecimientos);
router.get('/establecimiento/:id', administradorControllers.Establecimiento);

router.get('/establecimiento/clases/:id', administradorControllers.ClasesEstablecimiento);

router.get('/administrador/reporte1', administradorControllers.reporte1);
router.get('/administrador/reporte2', administradorControllers.reporte2);
router.get('/administrador/reporte3', administradorControllers.reporte3);
router.get('/administrador/reporte4', administradorControllers.reporte4);

module.exports = router;

/*
GET: enviar algo
POST: ingresos a la base de datos, registros de datos etc
DELETE: eliminar algo
PUT: actualizar algo
*/

//router.get('/setTurno',sillaControlers.setTurno);