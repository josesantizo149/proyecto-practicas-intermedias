const { Router } = require('express');
const router = Router();

const estudianteControllers = require('../controllers/estudiante.controllers');

router.get('/test', estudianteControllers.test)

//registration & login
router.post('/newUser', estudianteControllers.newUser)
router.get('/login/:email/:contra', estudianteControllers.oneUser)

//classes
router.get('/classes/:id_estudent', estudianteControllers.classes)
router.get('/class/:id_class', estudianteControllers.class)

//publications
router.get('/publications/:id_class', estudianteControllers.publications)
router.get('/publication/:id_publication', estudianteControllers.publication)

//comments
router.get('/comments/:id_publication', estudianteControllers.comments)
router.post('/comment', estudianteControllers.comment)

//establisments
router.get('/allEstablishments', estudianteControllers.allEstablishments)
router.get('/establishments/:nom', estudianteControllers.establishments)
router.get('/establishment/:id_establishment', estudianteControllers.establishment)

module.exports = router;

/*
GET: enviar algo
POST: ingresos a la base de datos, registros de datos etc
DELETE: eliminar algo
PUT: actualizar algo
*/

//router.get('/setTurno',sillaControlers.setTurno);