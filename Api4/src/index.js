const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, './.env')})
const app = require('./app');

app.listen(app.get('port'));

console.log('Server on port', app.get('port'))


// const schedule = require('node-schedule');

// const job = schedule.scheduleJob('10 * * * * *', function(){
//   console.log('The answer to life, the universe, and everything!');
// });
