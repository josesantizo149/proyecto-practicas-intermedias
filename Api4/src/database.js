// get the client
const mysql = require('mysql2');

// create the connection to database
const connection = mysql.createConnection({
  
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  
  /*host: 'localhost',
  port: '3306',
  user: 'root',
  password: '1234',
  database: 'practicas_proyecto',*/
  multipleStatements: true
})

let sql_initial = `CREATE SCHEMA IF NOT EXISTS INTERMEDIAS;
USE INTERMEDIAS;`

/*let sql_initial = `CREATE SCHEMA IF NOT EXISTS INTERMEDIAS;
USE practicas_proyecto;`*/

connection.query(sql_initial, function (err, result) {
  if (err) throw err;
  console.log("Database created and connected");
});

module.exports = connection;



/*

if (connection) console.log('Db is connected')


connection.query(
    'CREATE SCHEMA IF NOT EXISTS PRACTICA2DB',
    function(err, results) {
      console.log(results);
    }
  );

  connection.query(
    'USE PRACTICA2DB',
    function(err, results) {
      console.log(results);
    }
  );


  connection.execute(
    `CREATE TABLE IF NOT EXISTS PRACTICA2DB.CLIMA(
        idClima INT NOT NULL AUTO_INCREMENT,
        velocidadViento VARCHAR(200) INT NOT NULL,
        humedad VARCHAR(1000) INT NOT NULL,
        temperatura VARCHAR(200) INT NOT NULL,
        direccionViento VARCHAR(200) NOT NULL,
        cantidadLuz INT NOT NULL
        PRIMARY KEY (idClima))`,
    function(err, results, fields) {
      console.log('here',results);
    }
  );

*/