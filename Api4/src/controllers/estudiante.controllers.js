const db = require('../database')
const estudianteControllers = {};

//####### END POINT 1 #######################
estudianteControllers.test = async (req, res) => {
  db.query(
    'SELECT * FROM `ADMIN`',
    function (err, results) {
      return res.status(200).send(results);
    }
  );
}

//####### register new user #######################
estudianteControllers.newUser = async (req, res) => {
  cui = req.body.cui
  nombre = req.body.nombre
  apellido = req.body.apellido
  telefono = req.body.telefono
  correo = req.body.correo
  contra = req.body.contra
  aprobado = req.body.aprobado
  db.query(
    `INSERT INTO
    estudiante (cui,nombre,apellido,telefono,correo,contra,aprobado)
    VALUES (${cui},'${nombre}','${apellido}',${telefono},'${correo}','${contra}',${aprobado})`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send({ 'msg': 'student added' })
    }
  )
}

//####### get one user, login #######################
estudianteControllers.oneUser = async (req, res) => {
  email = req.params.email
  contra = req.params.contra
  db.query(
    `SELECT id_estudiante as id FROM estudiante WHERE correo = '${email}' and contra = '${contra}' AND aprobado = 1`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      if (result[0] == undefined) {
        return res.status(400).send({ 'msg': 'No user found' })
      } else {
        return res.status(200).send(result[0])
      }
    }
  )
}

//####### show assignated classes #######################
estudianteControllers.classes = async (req, res) => {
  id = req.params.id_estudent
  db.query(
    `SELECT c.id_clase, c.nombre, c.descripcion, c.seccion, e.nombre, c.nombre
    FROM clase AS c, establecimiento AS e, catedratico AS ca, asignacion AS a, estudiante AS es
    WHERE a.id_clase = c.id_clase AND
    c.id_establecimiento = e.id_establecimiento AND
    c.id_catedratico = ca.id_catedratico AND
    a.id_estudiante = es.id_estudiante AND
    es.id_estudiante = ${id}`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result)
    }
  )
}

//####### show class #######################
estudianteControllers.class = async (req, res) => {
  id = req.params.id_class
  db.query(
    `SELECT * FROM clase WHERE id_clase = ${id}`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      if (result[0] == undefined) {
        res.status(400).send({ 'msg': 'No class found' })
      } else {
        return res.status(200).send(result[0])
      }
    }
  )
}

//####### show publications #######################
estudianteControllers.publications = async (req, res) => {
  id = req.params.id_class
  db.query(
    `SELECT p.id_publicacion, p.titulo, p.contenido, p.fecha
    FROM publicacion as p, clase as c
    WHERE p.id_clase = c.id_clase AND
    c.id_clase = ${id}`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result)
    }
  )
}

//####### show publications #######################
estudianteControllers.publication = async (req, res) => {
  id = req.params.id_publication
  db.query(
    `SELECT * FROM publicacion WHERE id_publicacion = ${id}`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result[0])
    }
  )
}

//####### show comments #######################
estudianteControllers.comments = async (req, res) => {
  id = req.params.id_publication
  db.query(
    `(SELECT c.id_comentario, c.fecha, c.contenido, e.nombre, 'student' AS 'from'
    FROM comentario AS c, estudiante AS e
    WHERE c.id_estudiante = e.id_estudiante AND c.id_catedratico IS NULL AND
    c.id_publicacion = ${id})
    UNION
    (SELECT c.id_comentario, c.fecha, c.contenido, ca.nombre, 'professor' AS 'from'
    FROM comentario AS c, catedratico AS ca
    WHERE c.id_catedratico = ca.id_catedratico AND c.id_estudiante IS NULL AND
    c.id_publicacion = ${id})
    ORDER BY fecha`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result)
    }
  )
}

//####### comment #######################
estudianteControllers.comment = async (req, res) => {
  content = req.body.contenido
  id_pub = req.body.id_publicacion
  id_est = req.body.id_estudiante
  db.query(
    `INSERT INTO comentario (fecha,contenido,id_publicacion,id_estudiante) VALUES(current_timestamp(),'${content}',${id_pub},${id_est})`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send({ 'msg': 'comment added' })
    }
  )
}

//####### search all stablishments #######################
estudianteControllers.allEstablishments = async (req, res) => {
  nom = req.params.nom
  db.query(
    `SELECT * FROM establecimiento`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result)
    }
  )
}

//####### search stablishments #######################
estudianteControllers.establishments = async (req, res) => {
  nom = req.params.nom
  db.query(
    `SELECT * FROM establecimiento WHERE nombre LIKE '%${nom}%'`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      return res.status(200).send(result)
    }
  )
}

//####### one stablishment #######################
estudianteControllers.establishment = async (req, res) => {
  id = req.params.id_establishment
  db.query(
    `SELECT * FROM establecimiento WHERE id_establecimiento = ${id}`,
    function (err, result) {
      if (err) return res.status(400).send({ 'msg': err.sqlMessage })
      if (result[0] == undefined) {
        res.status(400).send({ 'msg': 'No establishment found' })
      }
      return res.status(200).send(result[0])
    }
  )
}

module.exports = estudianteControllers;

/*


  db.query(
    'INSERT INTO uso_silla (idUsuario, hora_inicio, hora_fin, peso, horas_uso, fecha_uso, distancia, dia) VALUES (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

  db.query(
    'call inUso (?,?,?,?,?,?,?,?)',
    [turn.idUsuario, turn.horaInicio, turn.horaFin, turn.peso, turn.horasUso, turn.fechaUso, turn.distancia, turn.dia],
    function (err, results, fields) {
      console.log(results); // results contains rows returned by server
      console.log(fields); // fields contains extra meta data about results, if available
    }
  );

console.log(fecha.getDate()) //Dia del mes
console.log(fecha.getDay()) //D:0L:1M:2M:3J:4V:5S:6
console.log(fecha.getMonth()) //E:0F:1M:2A:3M:4J:5J:6A:7S:8O:9N:10D:11
console.log(fecha.getFullYear())
console.log(fecha.getHours())
console.log(fecha.getMinutes())
console.log(fecha.getSeconds())
console.log()
console.log()

fecha1
2021-09-08T04:32:29.228Z
1631075549228


fecha2
2021-09-08T04:43:04.399Z
1631076184399

635171

minutos (fecha2-fecha1)/60000

horas (fecha2-fecha1)/


min: 10.5861833
horas: 0.1764363883333


var fecha = new Date();

    var timestamp = fecha.getTime().toString()

    console.log(fecha)
    console.log(fecha.getTime())
    console.log(fecha.getDay())
    console.log(fecha.getMonth()+1)
    console.log(fecha.getFullYear())
    */

/*
var f1 = 1631075549228
var f2 = 1631076184399
var min = (f2 - f1)/60000
var hora = min/60
console.log(min)
console.log(hora)

*/
