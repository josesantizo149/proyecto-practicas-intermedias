import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  url = 'http://34.133.190.65:4000';

  url2 = 'http://localhost:4002'; // por si trabaja el del usuario estudiante con otra url
  
  //student backend
  url4 = 'http://localhost:4003'

  constructor(private http: HttpClient) { }

  /***************** LOGIN *********************/

  loginAdministrador(email: string, password: string){
    let usuario = this.getFormLogin(email, password);
    return this.http.post(`${this.url}/login/administrador`, usuario);
  }

  loginEstablecimiento(email: string, password: string){
    let usuario = this.getFormLogin(email, password);
    return this.http.post(`${this.url}/login/establecimiento`, usuario);
  }

  loginCatedratico(email: string, password: string){
    let usuario = this.getFormLogin(email, password);
    return this.http.post(`${this.url}/login/catedratico`, usuario);
  }

  loginEstudiante(email: string, password: string){
    return this.http.get(`${this.url4}/student/login/${email}/${password}`);
  }

  private getFormLogin(correo: string, password: string): any{
    return {
      correo,
      password
    }
  }
  
  //



}
