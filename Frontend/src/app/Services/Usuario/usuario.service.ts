import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
  private adminActivo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private establecimientoActivo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private catedraticoActivo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private estudianteActivo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private nadieActivo: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) {
  }


  // Colocamos el usuario en Local Storage
  setUsuarioLocalStorage(usuario: any) {
    let usuario_string = JSON.stringify(usuario);
    localStorage.setItem('usuario', usuario_string);

    if (usuario.tipo === "administrador") {
      this.adminActivo.next(true);
    } else if (usuario.tipo === "establecimiento") {
      this.establecimientoActivo.next(true);
    }  else if (usuario.tipo === "catedratico") {
      this.catedraticoActivo.next(true);
    }  else if (usuario.tipo === "estudiante") {
      this.estudianteActivo.next(true);
    }
    this.nadieActivo.next(false);
  }

  // Retorna el usuario del local Storage
  getUsuarioLocalStorage() {
    let usuario = localStorage.getItem('usuario');
    if (usuario != null) {
      let usuario_json = JSON.parse(usuario);
      return usuario_json;
    }
    return null;
  }

  // retorna la llave primaria del usuario 
  getIdUsuario() {
    let usuario = localStorage.getItem('usuario');
    if (usuario != null) {
      let usuario_json = JSON.parse(usuario);
      return usuario_json.id;
    }
    return null;
  }


  // retorna el tipo de usuario 
  getTipoUsuario() : string{
    let usuario = localStorage.getItem('usuario');
    if (usuario != null) {
      let usuario_json = JSON.parse(usuario);
      return usuario_json.tipo;
    }
    return '';
  }



  // Cerrar sesion del usuario activo
  cerrarSesion() {
    this.nadieActivo.next(true);
    this.adminActivo.next(false);
    this.establecimientoActivo.next(false);
    this.catedraticoActivo.next(false);
    this.estudianteActivo.next(false);

    localStorage.removeItem('usuario');
    localStorage.removeItem('id_clase')
    localStorage.removeItem('id_publicacion')
    localStorage.removeItem('id_establishment')
    this.router.navigate(['/home']);
  }



  // OBSERVABLES

  get getAdminActivo() {
    return this.adminActivo.asObservable();
  }

  get getEstablecimientoActivo() {
    return this.establecimientoActivo.asObservable();
  }

  get getCatedraticoActivo() {
    return this.catedraticoActivo.asObservable();
  }

  get getEstudianteActivo() {
    return this.estudianteActivo.asObservable();
  }

  get getNadieActivo() {
    return this.nadieActivo.asObservable();
  }

  verificarActivo(){
    let user = localStorage.getItem('usuario');
    if (user != null) {
      let usuario = JSON.parse(user);      
      if (usuario.tipo === "administrador") {
        this.adminActivo.next(true);
      } else if (usuario.tipo === "establecimiento") {
        this.establecimientoActivo.next(true);
      }  else if (usuario.tipo === "catedratico") {
        this.catedraticoActivo.next(true);
      }  else if (usuario.tipo === "estudiante") {
        this.estudianteActivo.next(true);
      }
      this.nadieActivo.next(false);
      return;
    }
    this.nadieActivo.next(true);
    this.adminActivo.next(false);
    this.establecimientoActivo.next(false);
    this.catedraticoActivo.next(false);
    this.estudianteActivo.next(false);
  }

}
