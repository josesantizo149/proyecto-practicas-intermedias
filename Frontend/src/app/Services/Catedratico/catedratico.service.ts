import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatedraticoService {
  url = 'http://192.168.1.17:3000';

  constructor(private http: HttpClient) { }
}
