import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EstudianteService {
  //url = 'http://192.168.1.17:3000';
  url = 'http://localhost:4003';

  constructor(private http: HttpClient) { }

  //show asignates classes
  getClasses(id: number) {
    return this.http.get(`${this.url}/student/classes/${id}`)
  }

  //set ids in localstorage
  setIdLS(id: any, from: string) {
    let json_string = JSON.stringify(id);
    localStorage.setItem(from, json_string);
  }

  //get id from localstorage
  getIdLS(from: string) {
    let id = localStorage.getItem(from);
    if (id != null) {
      let id_json = JSON.parse(id);
      return id_json.id
    }
    return null;
  }

  //show publications from class
  getPublications(id: number) {
    return this.http.get(`${this.url}/student/publications/${id}`)
  }

  //show one publication
  getPublication(id: number) {
    return this.http.get(`${this.url}/student/publication/${id}`)
  }

  //show all comments from publication
  getComments(id: number) {
    return this.http.get(`${this.url}/student/comments/${id}`)
  }

  //if for comment
  getLastId() {
    return this.http.get(`${this.url}/student/lastid`)
  }

  //insert comment
  comment(comentario: any) {
    return this.http.post(`${this.url}/student/comment`, comentario)
  }

  //show all establishments
  allEstablishments() {
    return this.http.get(`${this.url}/student/allEstablishments`)
  }

  //search establishments
  searchEstablishments(filtro: string) {
    return this.http.get(`${this.url}/student/establishments/${filtro}`)
  }

  //one establishment
  establishment(id: number) {
    return this.http.get(`${this.url}/student/establishment/${id}`)
  }
}
