import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdministradorService {
  url = 'http://34.133.190.65:4000';

  // 34.133.190.65

  constructor(private http: HttpClient) { }

  /* ***************** REGISTRAR USUARIOS ******************** */

  public registroAdministrador(cui: number, nombre: string, apellido: string, correo: string, password: string){
    let usuario = {
      cui,
      nombre,
      apellido,
      correo, 
      password
    }
    return this.http.post(`${this.url}/registro/administrador`, usuario);
  }  

  public registroEstablecimiento(nombre: string, direccion: string, tipo: string, correo: string, password: string, biografia: string, estado: string){
    let establecimiento = {
      nombre,
      direccion,
      tipo,
      correo,
      password,
      bibliografia: biografia,
      estado
    };
    return this.http.post(`${this.url}/registro/establecimiento`, establecimiento);

  }

  public registroCatedratico(cui: number, nombre: string, apellido: string, telefono: string, correo: string, password: string, estado: string) {
    let catedratico = {
      cui,
      nombre,
      apellido,
      telefono,
      correo,
      password,
      estado
    };
    return this.http.post(`${this.url}/registro/catedratico`, catedratico);
  }

  public registroEstudiante(cui: number, nombre: string, apellido: string, telefono: string, correo: string, password: string, estado: string) {
    let estudiante = {
      cui,
      nombre,
      apellido,
      telefono,
      correo,
      password,
      estado
    };
    return this.http.post(`${this.url}/registro/estudiante`, estudiante);
  }



  /* ***************** SOLICITUDES ******************** */
  
  /* OBTENER SOLICITUDES */

  public getSolicitudesEstablecimiento() {
    return this.http.get(`${this.url}/solicitud/establecimiento`);
  }
  
  public getSolicitudesCatedratico() {
    return this.http.get(`${this.url}/solicitud/catedratico`);
  }
  
  public getSolicitudesEstudiante() {
    return this.http.get(`${this.url}/solicitud/estudiante`);
  }

  /* ACEPTAR O RECHAZAR SOLICITUDES */

  public accionSolicitudEstablecimiento(id: string, estado: string) {
    let accion = {
      id,
      estado
    }
    return this.http.put(`${this.url}/solicitud/accion/establecimiento`, accion);
  }

  public accionSolicitudCatedratico(cui: string, estado: string) {
    let accion = {
      cui,
      estado
    }
    return this.http.put(`${this.url}/solicitud/accion/catedratico`, accion);
  }

  public accionSolicitudEstudiante(cui: string, estado: string) {
    let accion = {
      cui,
      estado
    }
    return this.http.put(`${this.url}/solicitud/accion/estudiante`, accion);
  }


  /* ***************** OBTENER ESTABLECIMIENTOS ******************** */

  public getEstablecimientos() {
    return this.http.get(`${this.url}/establecimientos`);
  }

  /* ***************** OBTENER ESTABLECIMIENTO ESCOGIDO ******************** */
  public getEstablecimiento(id: string) {
    return this.http.get(`${this.url}/establecimiento/${id}`);
  }

  // Obtener clases del establecimiento escogido
  public getEstablecimientoClases(id: string) {
    return this.http.get(`${this.url}/establecimiento/clases/${id}`);
  }


  /* ***************** OBTENER REPORTES ******************** */
  public getReporte1() {
    return this.http.get(`${this.url}/administrador/reporte1`);
  }
  
  public getReporte2() {
    return this.http.get(`${this.url}/administrador/reporte2`);
  }
  
  public getReporte3() {
    return this.http.get(`${this.url}/administrador/reporte3`);
  }
  
  public getReporte4() {
    return this.http.get(`${this.url}/administrador/reporte4`);
  }

  /* ***************** CARGA MASIVA DE USUARIOS ******************** */
  public cargaMasivaAdministrador(form: FormData){
    return this.http.post(`${this.url}/cargamasiva/administrador`, form);
  }

  public cargaMasivaEstablecimiento(form: FormData){
    return this.http.post(`${this.url}/cargamasiva/establecimiento`, form);
  }

  public cargaMasivaCatedratico(form: FormData){
    return this.http.post(`${this.url}/cargamasiva/catedratico`, form);
  }

  public cargaMasivaEstudiante(form: FormData){
    return this.http.post(`${this.url}/cargamasiva/estudiante`, form);
  }


  /* *****************  ******************** */



}
