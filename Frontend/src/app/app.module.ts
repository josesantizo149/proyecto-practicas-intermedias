import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Servicios
import { HomeService } from './Services/Home/home.service';
import { AdministradorService } from './Services/Administrador/administrador.service';
import { EstablecimientoService } from './Services/Establecimiento/establecimiento.service';
import { CatedraticoService } from './Services/Catedratico/catedratico.service';
import { EstudianteService } from './Services/Estudiante/estudiante.service';
import { UsuarioService } from './Services/Usuario/usuario.service';

// Componentes
import { HomeComponent } from './Components/Home/home/home.component';
import { LoginComponent } from './Components/Home/login/login.component';
import { RegistrogComponent } from './Components/Home/registrog/registrog.component';
import { HomeAComponent } from './Components/Administrador/home-a/home-a.component';
import { HomeCComponent } from './Components/Catedratico/home-c/home-c.component';
import { HomeEComponent } from './Components/Establecimiento/home-e/home-e.component';
import { HomeSComponent } from './Components/Estudiante/home-s/home-s.component';
import { NavHomeComponent } from './Components/Home/nav-home/nav-home.component';
import { NavAdminComponent } from './Components/Administrador/nav-admin/nav-admin.component';
import { NavCatedraticoComponent } from './Components/Catedratico/nav-catedratico/nav-catedratico.component';
import { NavEstablecimientoComponent } from './Components/Establecimiento/nav-establecimiento/nav-establecimiento.component';
import { NavStudentComponent } from './Components/Estudiante/nav-student/nav-student.component';
import { FormRegEstablecComponent } from './Components/Home/FormsReg/form-reg-establec/form-reg-establec.component';
import { FormRegCatedratComponent } from './Components/Home/FormsReg/form-reg-catedrat/form-reg-catedrat.component';
import { FormRegStudentComponent } from './Components/Home/FormsReg/form-reg-student/form-reg-student.component';
import { SolicitudesAComponent } from './Components/Administrador/solicitudes-a/solicitudes-a.component';
import { RegistroMAComponent } from './Components/Administrador/registro-ma/registro-ma.component';
import { CargaAComponent } from './Components/Administrador/carga-a/carga-a.component';
import { ReEstablecimientosAComponent } from './Components/Administrador/re-establecimientos-a/re-establecimientos-a.component';
import { RepEstablecimientoAComponent } from './Components/Administrador/rep-establecimiento-a/rep-establecimiento-a.component';
import { ReportesAComponent } from './Components/Administrador/reportes-a/reportes-a.component';
import { RegAdminComponent } from './Components/Administrador/reg-admin/reg-admin.component';
import { ClaseComponent } from './Components/Estudiante/clase/clase.component';
import { CommentsComponent } from './Components/Estudiante/comments/comments.component';
import { SearchComponent } from './Components/Estudiante/search/search.component';
import { EstablishmentComponent } from './Components/Estudiante/establishment/establishment.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistrogComponent,
    HomeAComponent,
    HomeCComponent,
    HomeEComponent,
    HomeSComponent,
    NavHomeComponent,
    NavAdminComponent,
    NavCatedraticoComponent,
    NavEstablecimientoComponent,
    NavStudentComponent,
    FormRegEstablecComponent,
    FormRegCatedratComponent,
    FormRegStudentComponent,
    SolicitudesAComponent,
    RegistroMAComponent,
    CargaAComponent,
    ReEstablecimientosAComponent,
    RepEstablecimientoAComponent,
    ReportesAComponent,
    RegAdminComponent,
    ClaseComponent,
    CommentsComponent,
    SearchComponent,
    EstablishmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    HomeService,
    AdministradorService,
    EstablecimientoService,
    CatedraticoService,
    EstudianteService,
    UsuarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
