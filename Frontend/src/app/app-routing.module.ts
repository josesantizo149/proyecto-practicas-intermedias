import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CargaAComponent } from './Components/Administrador/carga-a/carga-a.component';
import { HomeAComponent } from './Components/Administrador/home-a/home-a.component';
import { ReEstablecimientosAComponent } from './Components/Administrador/re-establecimientos-a/re-establecimientos-a.component';
import { RegistroMAComponent } from './Components/Administrador/registro-ma/registro-ma.component';
import { RepEstablecimientoAComponent } from './Components/Administrador/rep-establecimiento-a/rep-establecimiento-a.component';
import { ReportesAComponent } from './Components/Administrador/reportes-a/reportes-a.component';
import { SolicitudesAComponent } from './Components/Administrador/solicitudes-a/solicitudes-a.component';
import { HomeCComponent } from './Components/Catedratico/home-c/home-c.component';
import { HomeEComponent } from './Components/Establecimiento/home-e/home-e.component';
import { ClaseComponent } from './Components/Estudiante/clase/clase.component';
import { CommentsComponent } from './Components/Estudiante/comments/comments.component';
import { EstablishmentComponent } from './Components/Estudiante/establishment/establishment.component';
import { HomeSComponent } from './Components/Estudiante/home-s/home-s.component';
import { SearchComponent } from './Components/Estudiante/search/search.component';
import { HomeComponent } from './Components/Home/home/home.component';
import { LoginComponent } from './Components/Home/login/login.component';
import { RegistrogComponent } from './Components/Home/registrog/registrog.component';

const routes: Routes = [
  { path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },

  // GENERAL
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'registro',
    component: RegistrogComponent,
  },


  // ADMINISTRADOR
  {
    path: 'administrador/home',
    component: HomeAComponent,
  },
  {
    path: 'administrador/solicitudes',
    component: SolicitudesAComponent,
  },
  {
    path: 'administrador/cargamasiva',
    component: CargaAComponent,
  },
  {
    path: 'administrador/registro',
    component: RegistroMAComponent,
  },
  {
    path: 'administrador/establecimientos',
    component: ReEstablecimientosAComponent,
  },
  {
    path: 'administrador/establecimiento/:id',
    component: RepEstablecimientoAComponent,
  },
  {
    path: 'administrador/reportes',
    component: ReportesAComponent,
  },


  // ESTABLECIMIENTO
  {
    path: 'establecimiento/home',
    component: HomeEComponent,
  },


  // CATEDRATICO
  {
    path: 'catedratico/home',
    component: HomeCComponent,
  },


  // ESTUDIANTE
  {
    path: 'estudiante/home',
    component: HomeSComponent,
  },
  {
    path: 'student/class',
    component: ClaseComponent
  },
  {
    path: 'student/comments',
    component: CommentsComponent
  },
  {
    path: 'student/search',
    component: SearchComponent
  },
  {
    path: 'student/establishment',
    component: EstablishmentComponent
  },


  // Error de ruta
  { path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
