import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';

@Component({
  selector: 'app-nav-establecimiento',
  templateUrl: './nav-establecimiento.component.html',
  styleUrls: ['./nav-establecimiento.component.css']
})
export class NavEstablecimientoComponent implements OnInit {
  activo = new Observable<boolean>();

  constructor(private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.usuarioService.verificarActivo();
    this.activo = this.usuarioService.getEstablecimientoActivo;
  }

  cerrarSesion(): void {
    this.usuarioService.cerrarSesion();
  }

}
