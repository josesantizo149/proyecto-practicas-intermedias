import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-reg-catedrat',
  templateUrl: './form-reg-catedrat.component.html',
  styleUrls: ['./form-reg-catedrat.component.css']
})

export class FormRegCatedratComponent implements OnInit {

  public registroCorrecto = false;

  registerForm = this.fb.group({
    cui: ['', [Validators.required, Validators.pattern('[0-9]+')]],
    // codigo: ['', [Validators.required, Validators.pattern('[0-9]+')]],
    nombre: ['', [Validators.required]],
    apellido: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(3)]],
  });
  
  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  constructor(
    private adminService: AdministradorService,
    private fb: FormBuilder,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
  }
  
  // verifica el control de error del registerForm
  get control() {
    return this.registerForm.controls;
  }


  registrar() {
    this.registroCorrecto = true;

    // Si tiene error, retorno
    if (this.registerForm.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Campos Invalidos',
        text: 'Complete los campos correctamente',
      });
      return;
    }

    const cui = Number(this.getTextForm('cui'));
    // const codigo = this.getTextForm('codigo');
    const nombre = this.getTextForm('nombre');
    const apellido = this.getTextForm('apellido');
    const telefono = this.getTextForm('telefono');
    let correo = this.getTextForm('email');
    correo = correo.replace(/ /g, '');
    const password = this.getTextForm('password');
    const estado = (this.usuarioService.getTipoUsuario() === 'administrador') ? "aprobado" : "pendiente";

    this.registroCorrecto = false;

    this.adminService.registroCatedratico(cui, nombre, apellido, telefono, correo, password, estado).subscribe(
      (res: any) => {
        if(!res.error) {
          if(estado === "aprobado") {
            this.alertaCorrecta('Catedrático Registrado Correctamente');
          } else {
            this.alertaCorrecta('Solicitud de registro enviada al sistema correctamente');
          }
          this.registerForm.reset();
        } else {
          this.alertaError("ERROR: "+res.message);
        }
      }, 
      (err: any) => {
        this.alertaError("ERROR: "+err.error.message);
      }
    );
    
  }
  
  alertaCorrecta(mensaje: string) {
    this.Toast.fire({
      icon: "success",
      title: mensaje,
    });
  }

  alertaError(mensaje: string){
    this.Toast.fire({
      icon: "error",
      title: mensaje,
    });
  }

  
  getTextForm(nombre: string): string {
    return this.registerForm.get(nombre)?.value;
  }

}
