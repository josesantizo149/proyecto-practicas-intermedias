import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HomeService } from 'src/app/Services/Home/home.service';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
  public loginCorrecto = false;
  loginForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(3)]],
    tipo: ['Administrador', Validators.required],
  });
  
  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  constructor(
    private homeService: HomeService,
    private formBuilder: FormBuilder,
    private router: Router,
    private modal: NgbModal,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit(): void {
  }

  // verifica el control de error del loginForm
  get control() {
    return this.loginForm.controls;
  }

  loginFormulario() {
    this.loginCorrecto = true;

    // Si tiene error, retorno
    if (this.loginForm.invalid) {
        this.alertaError(`¡Campos Incompletos!`);
      return;
    }

    let correo = this.getTextForm('email');
    correo = correo.replace(/ /g, '');
    const password = this.getTextForm('password');
    const tipo = this.getTextForm('tipo').toLowerCase();

    this.loginCorrecto = false;


    let usuario = {
      id: 1,
      tipo: tipo
    }
    // this.usuarioService.setUsuarioLocalStorage(usuario);
    
    if(usuario.tipo === "administrador") {
      this.loginAdministrador(correo, password);
    } else if(usuario.tipo === "establecimiento") {
      this.loginEstablecimiento(correo, password);
    } else if(usuario.tipo === "catedratico") {
      this.loginCatedratico(correo, password);
    } else if(usuario.tipo === "estudiante") {
      this.loginEstudiante(correo, password);
    }    
  }


  loginAdministrador(correo: string, password: string){
    this.homeService.loginAdministrador(correo, password).subscribe(
      (res : any) => {
        if(!res.error) {
          let usuario = {
            id: res.result[0].cui,
            nombre: res.result[0].nombre,
            apellido: res.result[0].apellido,
            correo: res.result[0].correo,
            tipo: "administrador"
          }
          this.usuarioService.setUsuarioLocalStorage(usuario);
          this.alertaIngresoCorrecto();        
          this.router.navigate(['/administrador/home']);
          this.loginForm.reset();
        } else {
          this.alertaError(`¡Credenciales Invalidas!`);
        }
      },
      (err: any) => {
        this.alertaError(`¡Credenciales Invalidas!`);
      }
    );
  }

  loginEstablecimiento(correo: string, password: string){
    this.homeService.loginEstablecimiento(correo, password).subscribe(
      (res: any) => {
        if(!res.error && res.result[0].estado === "aprobado") {
          let usuario = {
            id: res.result[0].id_establecimiento,
            nombre: res.result[0].nombre,
            correo: res.result[0].correo,
            tipo: "establecimiento"
          }
          this.usuarioService.setUsuarioLocalStorage(usuario);
          this.alertaIngresoCorrecto();
          this.router.navigate(['/establecimiento/home']);
          this.loginForm.reset();
        } else {
          this.alertaError(`¡Credenciales Invalidas!`);

        }
      },
      (err: any) => {
        this.alertaError(`¡Credenciales Invalidas!`);
      }
    );
  }

  loginCatedratico(correo: string, password: string){
    this.homeService.loginCatedratico(correo, password).subscribe(
      (res: any) => {
        if(!res.error && res.result[0].estado === "aprobado") {
          let usuario = {
            id: res.result[0].id_catedratico,
            cui: res.result[0].cui,
            nombre: res.result[0].nombre,
            apellido: res.result[0].apellido,
            correo: res.result[0].correo,
            tipo: "catedratico"
          }
          this.usuarioService.setUsuarioLocalStorage(usuario);
          this.alertaIngresoCorrecto();
          this.router.navigate(['/catedratico/home']);
          this.loginForm.reset();
        } else {
          this.alertaError(`¡Credenciales Invalidas!`);
        }
      },
      (err: any) => {

        this.alertaError(`¡Credenciales Invalidas!`);
      }
    );
  }

  loginEstudiante(correo: string, password: string){
    this.homeService.loginEstudiante(correo, password).subscribe(
      (res: any) => {
        let usuario = {
          id: res.id,
          tipo: 'estudiante'
        }
        this.usuarioService.setUsuarioLocalStorage(usuario);
        this.alertaIngresoCorrecto();
        this.router.navigate(['/estudiante/home']);
        this.loginForm.reset();
      },
      (err: any) => {
        this.alertaError(`¡Credenciales Invalidas!`);
      }
    );
  }


  alertaIngresoCorrecto() {
    this.Toast.fire({
      icon: "success",
      title: `¡Inició de Sesión Correctamente!`,
    });
  }

  alertaError(mensaje: string){
    this.Toast.fire({
      icon: "error",
      title: mensaje,
    });
  }

  
  getTextForm(nombre: string): string {
    return this.loginForm.get(nombre)?.value;
  }

}
