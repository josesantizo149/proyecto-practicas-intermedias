import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';

@Component({
  selector: 'app-nav-home',
  templateUrl: './nav-home.component.html',
  styleUrls: ['./nav-home.component.css']
})

export class NavHomeComponent implements OnInit {
  activo = new Observable<boolean>();


  constructor(private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.usuarioService.verificarActivo();
    this.activo = this.usuarioService.getNadieActivo;
  }

}
