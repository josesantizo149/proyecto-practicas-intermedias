import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-registrog',
  templateUrl: './registrog.component.html',
  styleUrls: ['./registrog.component.css']
})

export class RegistrogComponent implements OnInit {
  tipoUsuario: any = ['Establecimiento', 'Catedratico', 'Estudiante'];

  tipoUserForm = this.fb.group({
    tipo: this.tipoUsuario[0]
  });

  constructor(public fb: FormBuilder) { }

  ngOnInit(): void {
  }

  changeSuit(e : any) {
    this.tipoUserForm.get('tipo')?.setValue(e.target.value);
  }

}
