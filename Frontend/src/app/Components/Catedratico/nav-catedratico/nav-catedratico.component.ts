import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';

@Component({
  selector: 'app-nav-catedratico',
  templateUrl: './nav-catedratico.component.html',
  styleUrls: ['./nav-catedratico.component.css']
})
export class NavCatedraticoComponent implements OnInit {
  activo = new Observable<boolean>();

  constructor(private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.usuarioService.verificarActivo();
    this.activo = this.usuarioService.getCatedraticoActivo;
  }

  cerrarSesion(): void {
    this.usuarioService.cerrarSesion();
  }
  
}
