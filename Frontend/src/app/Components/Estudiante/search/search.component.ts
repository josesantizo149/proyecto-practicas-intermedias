import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstudianteService } from 'src/app/Services/Estudiante/estudiante.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private studentService: EstudianteService, private router: Router) { }

  establishments: any[] = []
  filtro: string = ''

  ngOnInit(): void {
    this.studentService.allEstablishments().subscribe((res: any) => {
      this.establishments = res
    })
  }

  search() {
    this.studentService.searchEstablishments(this.filtro).subscribe((res: any) => {
      this.establishments = res
    })
  }

  showEstablishment(id: number) {
    let id_ = {
      id: id
    }
    this.studentService.setIdLS(id_,'id_establishment')
    this.router.navigate(['/student/establishment'])
  }

}
