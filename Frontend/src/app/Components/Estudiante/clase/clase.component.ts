import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstudianteService } from 'src/app/Services/Estudiante/estudiante.service';

@Component({
  selector: 'app-clase',
  templateUrl: './clase.component.html',
  styleUrls: ['./clase.component.css']
})
export class ClaseComponent implements OnInit {

  constructor(private studentService: EstudianteService, private router: Router) { }

  publications: any[] = []
  id_clase: number = -1

  ngOnInit(): void {
    this.id_clase = this.studentService.getIdLS('id_clase')
    this.studentService.getPublications(this.id_clase).subscribe((res: any) => {
      this.publications = res
    })
  }

  comments(id: number){
    let id_ = {
      id: id
    }
    this.studentService.setIdLS(id_,'id_publicacion')
    this.router.navigate(['/student/comments'])
  }

}
