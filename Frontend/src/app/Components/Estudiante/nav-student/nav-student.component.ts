import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';

@Component({
  selector: 'app-nav-student',
  templateUrl: './nav-student.component.html',
  styleUrls: ['./nav-student.component.css']
})
export class NavStudentComponent implements OnInit {
  activo = new Observable<boolean>();


  constructor(private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.usuarioService.verificarActivo();
    this.activo = this.usuarioService.getEstudianteActivo;
  }

  cerrarSesion(): void {
    this.usuarioService.cerrarSesion();
  }

}
