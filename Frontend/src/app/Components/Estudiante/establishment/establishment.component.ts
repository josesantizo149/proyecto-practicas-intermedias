import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstudianteService } from 'src/app/Services/Estudiante/estudiante.service';

@Component({
  selector: 'app-establishment',
  templateUrl: './establishment.component.html',
  styleUrls: ['./establishment.component.css']
})
export class EstablishmentComponent implements OnInit {

  constructor(private studentService: EstudianteService, private router: Router) { }

  establishment = {
    id: -1,
    nombre: '',
    direccion: '',
    tipo: '',
    correo: '',
    contra: '',
    bibliografia: '',
    escudo: '',
    aprobado: ''
  }
  photos: any[] = []

  ngOnInit(): void {
    let id = this.studentService.getIdLS('id_establishment')
    this.studentService.establishment(id).subscribe((res: any) => {
      this.establishment = res
    })
  }

  ret(){
    localStorage.removeItem('id_establishment')
    this.router.navigate(['/student/search'])
  }

}
