import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EstudianteService } from 'src/app/Services/Estudiante/estudiante.service';

@Component({
  selector: 'app-home-s',
  templateUrl: './home-s.component.html',
  styleUrls: ['./home-s.component.css']
})
export class HomeSComponent implements OnInit {

  constructor(private studentService: EstudianteService, private router: Router) { }

  classes: any[] = []

  ngOnInit(): void {
    this.studentService.getClasses(1).subscribe((res: any) => {
      this.classes = res
    })
  }

  publications(id: number){
    let id_ = {
      id: id
    }
    this.studentService.setIdLS(id_,'id_clase')
    this.router.navigate(['/student/class'])
  }

}
