import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EstudianteService } from 'src/app/Services/Estudiante/estudiante.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  constructor(
    private studentService: EstudianteService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  publication = {
    id_publicacion: -1,
    titulo: '',
    fecha: '',
    contenido: ''
  }

  comments: any[] = []
  
  public commentCorrecto = false;
  commentForm = this.formBuilder.group({
    comentario: ['', [Validators.required, Validators.minLength(3)]]
  });
  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  ngOnInit(): void {
    let id = this.studentService.getIdLS('id_publicacion')
    this.studentService.getPublication(id).subscribe((res: any) => {
      this.publication = res
      console.log(res)
    })
    this.studentService.getComments(id).subscribe((res: any) => {
      this.comments = res
      console.log(res)
    })
  }

  get control() {
    return this.commentForm.controls;
  }

  comment(){
    this.commentCorrecto = true;
    // Si tiene error, retorno
    if (this.commentForm.invalid) {
        this.alertError(`¡Campos Incompletos!`);
      return;
    }
    let comentario = this.getTextForm('comentario');
    let id = this.studentService.getIdLS('id_publicacion')
    this.studentService.getLastId().subscribe((res: any) => {
      let id_comment = res
      let comment = {
        id_comentario: id_comment+1,
        contenido: comentario,
        id_publicacion: this.publication.id_publicacion,
        id_estudiante: id
      }
      this.studentService.comment(comment).subscribe()
      //this.alertSuccess()
      window.location.reload()
    })
  }

  alertSuccess() {
    this.Toast.fire({
      icon: "success",
      title: `Comentario agregado!`,
    });
  }

  alertError(mensaje: string){
    this.Toast.fire({
      icon: "error",
      title: mensaje,
    });
  }
  
  getTextForm(nombre: string): string {
    return this.commentForm.get(nombre)?.value;
  }

}
