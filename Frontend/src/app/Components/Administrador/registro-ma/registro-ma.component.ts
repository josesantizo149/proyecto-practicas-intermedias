import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-registro-ma',
  templateUrl: './registro-ma.component.html',
  styleUrls: ['./registro-ma.component.css']
})

export class RegistroMAComponent implements OnInit {
  tipoUsuario: any = ['Administrador','Establecimiento', 'Catedratico', 'Estudiante'];

  tipoUserForm = this.fb.group({
    tipo: this.tipoUsuario[0]
  });

  constructor(public fb: FormBuilder) { }

  ngOnInit(): void {
  }

  changeSuit(e : any) {
    this.tipoUserForm.get('tipo')?.setValue(e.target.value);
  }

}
