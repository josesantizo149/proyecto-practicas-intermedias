import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-carga-a',
  templateUrl: './carga-a.component.html',
  styleUrls: ['./carga-a.component.css']
})
export class CargaAComponent implements OnInit {
  public registroCorrecto = false;
  public tipoUsuario: any = ['Administrador','Establecimiento', 'Catedratico', 'Estudiante'];
  public file: any = [];  
  public formCarga = this.fb.group({
    tipo: this.tipoUsuario[0],
    archivo: ['', [Validators.required]],
  });
  public loading : boolean = false;

  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  constructor(public fb: FormBuilder, private adminService: AdministradorService) { }

  ngOnInit(): void {
    this.loading = false;
  }

  changeSuit(e : any) {
    this.formCarga.get('tipo')?.setValue(e.target.value);
  }

  capturarFile(event: any): any{
    const archivoEscogido = event.target.files[0];
    this.file = archivoEscogido; 
    console.log(this.file);
    this.formCarga.get('archivo')?.setValue("HOLA");    

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      // console.log(fileReader.result);
      let a : string[] | undefined = fileReader.result?.toString().split("\n");
      console.log(a)
      if (a != undefined &&  a?.length > 0) {
        console.log(a[0])
        let b = a[0].split(";")
        console.log(b)
        console.log(b[0])
      }
    };
    fileReader.readAsText(this.file);
  }

  // verifica el control de error del registerForm
  get control() {
    return this.formCarga.controls;
  }

  
  cargaMasiva() {
    this.registroCorrecto = true;

    // Si tiene error, retorno
    if (this.formCarga.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Archivo No Subido',
        text: 'Suba el archivo con extensión (.csv) ',
      });
      return;
    }    

    this.registroCorrecto = false;
    return;

    try {
      this.loading = true;
      const formularioDeDatos = new FormData();
      formularioDeDatos.append('file', this.file);

      console.log(formularioDeDatos);

      if(this.getTextForm('tipo') === this.tipoUsuario[0]) { // Admin
        this.adminService.cargaMasivaAdministrador(formularioDeDatos).subscribe(
          (res: any) => {
            this.alertaCorrecta('Carga Masiva Realiza Correctamente');
            this.loading = false;
          }, 
          (err: any) => {
            this.alertaError("Error en la carga masiva");
            this.loading = false;
          }
        );

      } else if(this.getTextForm('tipo') === this.tipoUsuario[1]) { // Establecimiento
        this.adminService.cargaMasivaEstablecimiento(formularioDeDatos).subscribe(
          (res: any) => {
            this.alertaCorrecta('Carga Masiva Realiza Correctamente');
            this.loading = false;
          }, 
          (err: any) => {
            this.alertaError("Error en la carga masiva");
            this.loading = false;
          }
        );

      } else if(this.getTextForm('tipo') === this.tipoUsuario[2]) { // Catedrático
        this.adminService.cargaMasivaCatedratico(formularioDeDatos).subscribe(
          (res: any) => {
            this.alertaCorrecta('Carga Masiva Realiza Correctamente');
            this.loading = false;
          }, 
          (err: any) => {
            this.alertaError("Error en la carga masiva");
            this.loading = false;
          }
        );

      } else if(this.getTextForm('tipo') === this.tipoUsuario[3]) { // Estudiante
        this.adminService.cargaMasivaEstudiante(formularioDeDatos).subscribe(
          (res: any) => {
            this.alertaCorrecta('Carga Masiva Realiza Correctamente');
            this.loading = false;
          }, 
          (err: any) => {
            this.alertaError("Error en la carga masiva");
            this.loading = false;
          }
        );

      }

    } catch (error) {
      console.log(error);
      this.loading = false;
    }

  }




  
  getTextForm(nombre: string): string {
    return this.formCarga.get(nombre)?.value;
  }

  alertaCorrecta(mensaje: string) {
    this.Toast.fire({
      icon: "success",
      title: mensaje,
    });
  }

  alertaError(mensaje: string){
    this.Toast.fire({
      icon: "error",
      title: mensaje,
    });
  }



}
