import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioService } from 'src/app/Services/Usuario/usuario.service';

@Component({
  selector: 'app-nav-admin',
  templateUrl: './nav-admin.component.html',
  styleUrls: ['./nav-admin.component.css']
})
export class NavAdminComponent implements OnInit {
  activo = new Observable<boolean>();

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.usuarioService.verificarActivo();
    this.activo = this.usuarioService.getAdminActivo;
  }

  cerrarSesion(): void {
    this.usuarioService.cerrarSesion();
  }
  
}
