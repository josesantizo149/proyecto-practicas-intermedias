import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reg-admin',
  templateUrl: './reg-admin.component.html',
  styleUrls: ['./reg-admin.component.css']
})
export class RegAdminComponent implements OnInit {

  public registroCorrecto = false;

  registerForm = this.fb.group({
    cui: ['', [Validators.required, Validators.pattern('[0-9]+')]],
    nombre: ['', [Validators.required]],
    apellido: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(3)]],
  });
  
  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });


  constructor(
    private adminService: AdministradorService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
  }
  
  // verifica el control de error del registerForm
  get control() {
    return this.registerForm.controls;
  }


  registrar() {
    this.registroCorrecto = true;

    // Si tiene error, retorno
    if (this.registerForm.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Campos Invalidos',
        text: 'Complete los campos correctamente',
      });
      return;
    }

    const cui = Number(this.getTextForm('cui'));
    const nombre = this.getTextForm('nombre');
    const apellido = this.getTextForm('apellido');
    let correo = this.getTextForm('email');
    correo = correo.replace(/ /g, '');
    const password = this.getTextForm('password');

    this.registroCorrecto = false;

    this.adminService.registroAdministrador(cui, nombre, apellido, correo, password).subscribe(
      (res: any) => {
        if(!res.error) {
          this.alertaCorrecta('Administrador Registrado Correctamente');
          this.registerForm.reset();
        } else {
          this.alertaError("ERROR: "+res.message);
        }
      }, 
      (err: any) => {
        this.alertaError("ERROR: "+err.error.message);
      }
    );


  }

  alertaCorrecta(mensaje: string) {
    this.Toast.fire({
      icon: "success",
      title: mensaje,
    });
  }

  alertaError(mensaje: string){
    this.Toast.fire({
      icon: "error",
      title: mensaje,
    });
  }

  
  getTextForm(nombre: string): string {
    return this.registerForm.get(nombre)?.value;
  }

}
