import { Component, OnInit } from '@angular/core';
import { Chart, registerables} from 'chart.js';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';


@Component({
  selector: 'app-reportes-a',
  templateUrl: './reportes-a.component.html',
  styleUrls: ['./reportes-a.component.css']
})
export class ReportesAComponent implements OnInit {
  public chartReporte1: any = [];
  public chartReporte2: any = [];
  public chartReporte3: any = [];
  public chartReporte4: any = [];
  public flagReporte1: boolean = false;
  public flagReporte2: boolean = false;
  public flagReporte3: boolean = false;
  public flagReporte4: boolean = false;


  constructor(private adminService: AdministradorService) {
    Chart.register(...registerables); 
  }

  ngOnInit(): void {
    this.flagReporte1 = this.flagReporte2 = this.flagReporte3 = this.flagReporte4 = false;
    this.getInfoReporte1();
    this.getInfoReporte2();
    this.getInfoReporte3();
    this.getInfoReporte4();
  }


  public getInfoReporte1(): void {
    this.adminService.getReporte1().subscribe(
      (res: any) => {
        if(!res.error) {          
          let labels:any[] = [];
          let data:any[] = [];
          res.result.forEach((element:any) => {
            labels.push(element.establecimiento);
            data.push(element.cantidad);
          });
          this.flagReporte1 = true;
          this.chartReporte1 = new Chart('chartReporte1', {
              type: 'bar',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Cantidad de Clases Registradas',
                      data: data,
                      backgroundColor: 'rgba(54, 162, 235, 20)',
                      borderColor: 'rgba(153, 102, 255, 20)',
                      borderWidth: 2,
                  }]
                  
              },
              options: {
                  scales: {
                      y: {
                        beginAtZero: true,
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      },
                      x: {
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      }
                      
                  },
                  plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 20,
                                weight: 'bold'
                            },
                            color: 'white'
                        }
                    }
                  }
              }
          });
        } else {
          console.log("Error Info Reporte1 = ", res.message);
          this.chartReporte1 = [];
          this.flagReporte1 = false;
        }
      }, 
      (err: any) => {
        console.log("Error Info Reporte1 = ", err.error.message);
        this.chartReporte1 = [];
        this.flagReporte1 = false;
      }
    )
  }

  public getInfoReporte2(): void {
    this.adminService.getReporte2().subscribe(
      (res: any) => {
        if(!res.error) {          
          let labels:any[] = [];
          let data:any[] = [];
          res.result.forEach((element:any) => {
            labels.push(element.establecimiento);
            data.push(element.cantidad);
          });
          this.flagReporte2 = true;
          this.chartReporte2 = new Chart('chartReporte2', {
              type: 'bar',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Cantidad de Alumnos Registrados',
                      data: data,
                      backgroundColor: 'rgba(54, 162, 235, 20)',
                      borderColor: 'rgba(153, 102, 255, 20)',
                      borderWidth: 2,
                  }]
                  
              },
              options: {
                  scales: {
                      y: {
                        beginAtZero: true,
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      },
                      x: {
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      }
                      
                  },
                  plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 20,
                                weight: 'bold'
                            },
                            color: 'white'
                        }
                    }
                  }
              }
          });
        } else {
          console.log("Error Info Reporte2 = ", res.message);
          this.chartReporte2 = [];
          this.flagReporte2 = false;
        }
      }, 
      (err: any) => {
        console.log("Error Info Reporte2 = ", err.error.message);
        this.chartReporte2 = [];
        this.flagReporte2 = false;
      }
    )
  }

  public getInfoReporte3(): void {
    this.adminService.getReporte3().subscribe(
      (res: any) => {
        if(!res.error) {          
          let labels:any[] = [];
          let data:any[] = [];
          res.result.forEach((element:any) => {
            labels.push(element.nombre);
            data.push(element.cantidad);
            });
          this.flagReporte3 = true;
          this.chartReporte3 = new Chart('chartReporte3', {
              type: 'bar',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Cantidad de Clases Registradas',
                      data: data,
                      backgroundColor: 'rgba(54, 162, 235, 20)',
                      borderColor: 'rgba(153, 102, 255, 20)',
                      borderWidth: 2,
                  }]
                  
              },
              options: {
                  scales: {
                      y: {
                        beginAtZero: true,
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      },
                      x: {
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      }
                      
                  },
                  plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 20,
                                weight: 'bold'
                            },
                            color: 'white'
                        }
                    }
                  }
              }
          });
        } else {
          console.log("Error Info Reporte3 = ", res.message);
          this.chartReporte3 = [];
          this.flagReporte3 = false;
        }
      }, 
      (err: any) => {
        console.log("Error Info Reporte3 = ", err.error.message);
        this.chartReporte3 = [];
        this.flagReporte3 = false;
      }
    )
  }

  public getInfoReporte4(): void {
    this.adminService.getReporte4().subscribe(
      (res: any) => {
        if(!res.error) {          
          let labels:any[] = [];
          let data:any[] = [];
          res.result.forEach((element:any) => {
            labels.push(element.nombre);
            data.push(element.cantidad);
            });
          this.flagReporte4 = true;
          this.chartReporte4 = new Chart('chartReporte4', {
              type: 'bar',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Cantidad de Alumnos Registrados',
                      data: data,
                      backgroundColor: 'rgba(54, 162, 235, 20)',
                      borderColor: 'rgba(153, 102, 255, 20)',
                      borderWidth: 2,
                  }]                
              },
              options: {
                  scales: {
                      y: {
                        beginAtZero: true,
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      },
                      x: {
                        ticks: {
                          font: {
                            size: 20,
                            weight: 'bold'
                          },
                          color: 'white'
                        }
                      }
                      
                  },
                  plugins: {
                    legend: {
                        labels: {
                            // This more specific font property overrides the global property
                            font: {
                                size: 20,
                                weight: 'bold'
                            },
                            color: 'white'
                        }
                    }
                  }
              }
          });
        } else {
          console.log("Error Info Reporte4 = ", res.message);
          this.chartReporte4 = [];
          this.flagReporte4 = false;
        }
      }, 
      (err: any) => {
        console.log("Error Info Reporte4 = ", err.error.message);
        this.chartReporte4 = [];
        this.flagReporte4 = false;
      }
    )
  }



}
