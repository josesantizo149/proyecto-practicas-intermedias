import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';

@Component({
  selector: 'app-rep-establecimiento-a',
  templateUrl: './rep-establecimiento-a.component.html',
  styleUrls: ['./rep-establecimiento-a.component.css']
})
export class RepEstablecimientoAComponent implements OnInit {
  private id: string = "";
  public establecimiento: any = {};
  public clases: any = [];


  constructor(private activatedRoute: ActivatedRoute, private adminService: AdministradorService) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    console.log(this.id);
    this.getInfoEstablecimiento();
    this.getClases();
  }

  public getInfoEstablecimiento(): void {
    this.adminService.getEstablecimiento(this.id).subscribe(
      (res: any) => {
        if(!res.error) {
          this.establecimiento = res.result[0];
        } else {
          console.log("Error get Info Establecimiento = ", res.message);
          this.establecimiento = {};
        }
      }, 
      (err: any) => {
        console.log("Error get Info Establecimiento = ", err.error.message);
        this.establecimiento = {};
      }
    )
  }

  public getClases(): void {
    this.adminService.getEstablecimientoClases(this.id).subscribe(
      (res: any) => {
        if(!res.error) {
          this.clases = res.result;
        } else {
          console.log("Error get Info Clases = ", res.message);
          this.clases = [];
        }
      }, 
      (err: any) => {
        console.log("Error get Info Clases = ", err.error.message);
        this.clases = [];
      }
    )
  }



}
