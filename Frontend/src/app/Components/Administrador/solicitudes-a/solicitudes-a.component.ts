import { Component, OnInit } from '@angular/core';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-solicitudes-a',
  templateUrl: './solicitudes-a.component.html',
  styleUrls: ['./solicitudes-a.component.css']
})
export class SolicitudesAComponent implements OnInit {
  public solicitudesEstablecimiento: any = [];
  public solicitudesCatedratico: any = [];
  public solicitudesEstudiante: any = [];
  
  Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

  constructor(private adminService: AdministradorService) { }

  ngOnInit(): void {
    this.getSolicitudesEstablecimiento();
    this.getSolicitudesCatedratico();
    this.getSolicitudesEstudiante();
  }

  public getSolicitudesEstablecimiento(): void {
    this.adminService.getSolicitudesEstablecimiento().subscribe(
      (res: any) => {
        if(!res.error) {
          this.solicitudesEstablecimiento = res.result;
        } else {
          console.log("Error Solicitudes Establecimiento = ", res.message);
          this.solicitudesEstablecimiento = [];
        }
      }, 
      (err: any) => {
        console.log("Error Solicitudes Establecimiento = ", err.error.message);
        this.solicitudesEstablecimiento = [];
      }
    )
  }

  public getSolicitudesCatedratico(): void {
    this.adminService.getSolicitudesCatedratico().subscribe(
      (res: any) => {        
        if(!res.error) {
          this.solicitudesCatedratico = res.result;
        } else {
          console.log("Error Solicitudes Catetradicto = ", res.message);
          this.solicitudesCatedratico = [];
        }
      }, 
      (err: any) => {
        console.log("Error Solicitudes Catetradicto = ", err.error.message);
        this.solicitudesCatedratico = [];
      }
    )
  }

  public getSolicitudesEstudiante(): void {
    this.adminService.getSolicitudesEstudiante().subscribe(
      (res: any) => { 
        if(!res.error) {
          this.solicitudesEstudiante = res.result;
        } else {
          console.log("Error Solicitudes Estudiante = ", res.message);
          this.solicitudesEstudiante = [];
        }
      }, 
      (err: any) => {
        console.log("Error Solicitudes Estudiante = ", err.error.message);
        this.solicitudesEstudiante = [];
      }
    )
  }

  public aceptarEstablecimiento(codigo: string): void {
    this.adminService.accionSolicitudEstablecimiento(codigo, "aprobado").subscribe(
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesEstablecimiento();
          this.alertaCorrecta('Solicitud Establecimiento Aceptado Correctamente');
        } else {
          console.log("Error Aceptar Establecimiento = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Aceptar Establecimiento = ", err.error.message);
      }
    );
  }

  public rechazarEstablecimiento(codigo: string): void {
    this.adminService.accionSolicitudEstablecimiento(codigo, "rechazado").subscribe(
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesEstablecimiento();
          this.alertaCorrecta('Solicitud Establecimiento Rechazado Correctamente');
        } else {
          console.log("Error Rechazar Establecimiento = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Rechazar Establecimiento = ", err.error.message);
      }
    );
  }

  public aceptarCatedratico(cui: string): void {
    this.adminService.accionSolicitudCatedratico(cui, "aprobado").subscribe(
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesCatedratico();
          this.alertaCorrecta('Solicitud Catedrático Aceptado Correctamente');
        } else {
          console.log("Error Aceptar Catedrático = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Aceptar Catedrático = ", err.error.message);
      }
    );
  }

  public rechazarCatedratico(cui: string): void {
    this.adminService.accionSolicitudCatedratico(cui, "rechazado").subscribe(        
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesCatedratico();
          this.alertaCorrecta('Solicitud Catedrático Rechazado Correctamente');
        } else {
          console.log("Error Reachazar Catedrático = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Reachazar Catedrático = ", err.error.message);
      }
    );
  }


  public aceptarEstudiante(cui: string): void {
    this.adminService.accionSolicitudEstudiante(cui, "aprobado").subscribe(
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesEstudiante();
          this.alertaCorrecta('Solicitud Estudiante Aceptado Correctamente');   
        } else {
          console.log("Error Aceptar Estudiante = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Aceptar Estudiante = ", err.error.message);
      }
    );
  }

  public rechazarEstudiante(cui: string): void {
    this.adminService.accionSolicitudEstudiante(cui, "rechazado").subscribe(
      (res : any) => {
        if(!res.error) {
          this.getSolicitudesEstudiante();
          this.alertaCorrecta('Solicitud Estudiante Rechazado Correctamente');
        } else {
          console.log("Error Reachazar Estudiante = ", res.message);
        }
      }, 
      (err) => {
        console.log("Error Reachazar Estudiante = ", err.error.message);
      }
    );
  }

  alertaCorrecta(mensaje: string) {
    this.Toast.fire({
      icon: "success",
      title: mensaje,
    });
  }


}
