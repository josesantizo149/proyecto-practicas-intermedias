import { Component, OnInit } from '@angular/core';
import { AdministradorService } from 'src/app/Services/Administrador/administrador.service';

@Component({
  selector: 'app-re-establecimientos-a',
  templateUrl: './re-establecimientos-a.component.html',
  styleUrls: ['./re-establecimientos-a.component.css']
})
export class ReEstablecimientosAComponent implements OnInit {
  public establecimientos: any = [];

  constructor(private adminService: AdministradorService) { }

  ngOnInit(): void {
    this.getAllEstablecimientos();
  }


  public getAllEstablecimientos(): void {
    this.adminService.getEstablecimientos().subscribe(
      (res: any) => {
        if(!res.error) {
          this.establecimientos = res.result;
        } else {
          console.log("Error get Establecimientos = ", res.message);
          this.establecimientos = [];
        }
      }, 
      (err: any) => {
        console.log("Error get Establecimientos = ", err.error.message);
        this.establecimientos = [];
      }
    )
  }


}
