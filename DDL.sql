create table admin(
cui int not null auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
correo varchar(50) not null,
contra varchar(50) not null,
primary key (cui)
);

create table establecimiento(
id_establecimiento int not null auto_increment,
nombre varchar(50) not null,
direccion varchar(50) not null,
tipo varchar(50) not null,
correo varchar(50) not null,
contra varchar(50) not null,
bibliografia varchar(280) not null,
escudo varchar(50) not null,
aprobado boolean not null,
primary key (id_establecimiento)
);

create table catedratico(
id_catedratico int not null auto_increment,
cui int not null,
nombre varchar(50) not null,
apellido varchar(50) not null,
telefono int not null,
correo varchar(50) not null,
contra varchar(50) not null,
aprobado boolean not null,
primary key (id_catedratico)
);

create table foto_establecimiento(
id_foto int not null auto_increment,
foto varchar(50) not null,
id_establecimiento int not null,
primary key (id_foto),
foreign key (id_establecimiento) references establecimiento (id_establecimiento)
);

create table mensaje(
id_mensaje int not null auto_increment,
texto varchar(280) not null,
id_establecimiento int not null,
id_catedratico int not null,
primary key (id_mensaje),
foreign key (id_establecimiento) references establecimiento (id_establecimiento),
foreign key (id_catedratico) references catedratico (id_catedratico)
);

create table clase(
id_clase int not null auto_increment,
nombre varchar(50) not null,
descripcion varchar(280) not null,
seccion varchar(2) not null,
aprobado boolean not null,
id_establecimiento int not null,
id_catedratico int not null,
primary key (id_clase),
foreign key (id_establecimiento) references establecimiento (id_establecimiento),
foreign key (id_catedratico) references catedratico (id_catedratico)
);

create table estudiante(
id_estudiante int not null auto_increment,
cui int not null,
nombre varchar(50) not null,
apellido varchar(50) not null,
telefono int not null,
correo varchar(50) not null,
contra varchar(50) not null,
aprobado boolean,
primary key (id_estudiante)
);

create table asignacion(
id_asignacion int not null auto_increment,
fecha timestamp not null,
id_clase int not null,
id_estudiante int not null,
primary key (id_asignacion,id_clase,id_estudiante),
foreign key (id_clase) references clase (id_clase),
foreign key (id_estudiante) references estudiante (id_estudiante)
);

create table publicacion(
id_publicacion int not null auto_increment,
titulo varchar(50) not null,
contenido varchar(280) not null,
fecha timestamp not null,
id_clase int not null,
primary key (id_publicacion),
foreign key (id_clase) references clase (id_clase)
);

create table comentario(
id_comentario int not null auto_increment,
fecha timestamp not null,
contenido varchar(280) not null,
id_publicacion int not null,
id_estudiante int not null,
primary	key (id_comentario),
foreign key (id_publicacion) references publicacion (id_publicacion),
foreign key (id_estudiante) references publicacion (id_publicacion)
);

create table archivo(
id_archivo int not null auto_increment,
nombre varchar(50) not null,
id_publicacion int not null,
primary key (id_archivo),
foreign key (id_publicacion) references publicacion (id_publicacion)
);