const { Router } = require('express');
const router = Router();

const estudianteControllers = require('../controllers/estudiante.controllers');

router.get('/test',estudianteControllers.test);

module.exports = router;

/*
GET: enviar algo
POST: ingresos a la base de datos, registros de datos etc
DELETE: eliminar algo
PUT: actualizar algo
*/

//router.get('/setTurno',sillaControlers.setTurno);